'use strict'

let apikey = '1b0592056f8142cf9091533f2e0853ed';
let entry = 'https://newsapi.org/v2/';

let controllers = {
    getNewsDataTop: async (req, res) => {
        
        let countryTop = '';
        let categoryTop = '';
        let pageSizeTop = '';            
        let wordTop = req.body.wordTop;

        //check if a body parameter exists

        if(req.body.countryTop){
            countryTop = req.body.countryTop;
        }

        if(req.body.categoryTop){
            categoryTop = req.body.categoryTop;
        }

        if(req.body.pageSizeTop){
            pageSizeTop = req.body.pageSizeTop;
        }

        //Create url for fetch

        let url = entry+'top-headlines?q='+wordTop;

        if(countryTop != ''){
            url += '&country='+countryTop;
        }

        if(categoryTop != ''){
            url += '&category='+categoryTop;
        }

        if(pageSizeTop != ''){
            url += '&pageSize='+pageSizeTop;
        }

        url += '&apikey='+apikey;

        let data = await fetch(url)
            .then(data => {return data.json();})
        
        return res.status(200).send(data);
    },

    getNewsDataEvery: async (req, res) => {
        let wordEvery = req.body.wordEvery;
        let domainsEvery = ''
        let fromEvery = ''
        let toEvery = ''
        let langEvery = ''
        let pageSizeEvery = ''
        let sortEvery = ''  

        //check if a body parameter exists

        if(req.body.domainsEvery){
            domainsEvery = req.body.domainsEvery;
        }

        if(req.body.fromEvery){
            fromEvery = req.body.fromEvery;
        }

        if(req.body.toEvery){
            toEvery = req.body.toEvery;
        }

        if(req.body.langEvery){
            langEvery = req.body.langEvery;
        }

        if(req.body.pageSizeEvery){
            pageSizeEvery = req.body.pageSizeEvery;
        }

        if(req.body.sortEvery){
            sortEvery = req.body.sortEvery;
        }

        //Create url for fetch

        let url = entry+'everything?q='+wordEvery;

        if(domainsEvery != ''){
            url += '&domains='+domainsEvery
        }

        if(fromEvery != ''){
            url += '&from='+fromEvery
        }

        if(toEvery != ''){
            url += '&to='+toEvery
        }

        if(langEvery != ''){
            url += '&language='+langEvery
        }

        if(pageSizeEvery != ''){
            url += '&pageSize='+pageSizeEvery
        }

        if(sortEvery != ''){
            url += '&sortBy='+sortEvery
        }

        url += '&apikey='+apikey;

        let data = await fetch(url)
            .then(data => {return data.json();})
    
        return res.status(200).send(data);
    }
};

module.exports = controllers;