'use strict'

let express = require('express');
let app = express();
let bodyParser = require('body-parser');

let route = require('../route/route');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    res.header('Allow, GET, POST, PUT, DELETE, OPTIONS');
    next();
});

app.use('/api', route);

module.exports = app;
