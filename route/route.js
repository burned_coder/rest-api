let express = require('express');
let router = express.Router();
let controller = require('../controllers/controller');

router.post('/getNewsDataTop', controller.getNewsDataTop);
router.post('/getNewsDataEvery', controller.getNewsDataEvery);

module.exports = router;